"use strict"
// Теоретичні питання
// 1) Для визначення натиснутої клавіші користувачем використовують властивості event.key або event.keyCode.
// 2) event.key повертає символ натиснутої клавіші, а event.code - унікальний ідентифікатор клавіші.
// 3) Три основні події клавіатури - keydown, keyup і keypress. keydown та keyup спрацьовують 
// під час натискання та відпускання клавіші відповідно. keypress спрацьовує, коли клавіша натиснута, і символ повинен бути введений.

// Практичне Завдання 
document.addEventListener('DOMContentLoaded', function () {
    const buttons = document.querySelectorAll('.btn');
    let activeButton = null;

    function activateButton(key) {
        buttons.forEach(button => {
            if (button.textContent === key) {
                button.classList.add('active');
                activeButton = button;
            } else {
                button.classList.remove('active');
            }
        });
    }

    document.addEventListener('keydown', function (event) {
        const keyCode = event.keyCode || event.which;
        const key = String.fromCharCode(keyCode).toUpperCase();
        activateButton(key);
    });

    buttons.forEach(button => {
        button.addEventListener('click', function () {
            const key = this.textContent;
            activateButton(key);
        });
    });
});document.addEventListener('DOMContentLoaded', function () {
    const buttons = document.querySelectorAll('.btn');
    let activeButton = null;

    function activateButton(key) {
        buttons.forEach(button => {
            if (button.textContent === key) {
                button.classList.add('active');
                activeButton = button;
            } else {
                button.classList.remove('active');
            }
        });
    }

    document.addEventListener('keydown', function (event) {
        const keyCode = event.keyCode || event.which;
        let key;
        switch (keyCode) {
            case 13: // Enter
                key = "Enter";
                break;
            case 9: // Tab
                key = "Tab";
                break;
            case 83: // S
                key = "S";
                break;
            case 69: // E
                key = "E";
                break;
            case 79: // O
                key = "O";
                break;
            case 78: // N
                key = "N";
                break;
            case 76: // L
                key = "L";
                break;
            case 90: // Z
                key = "Z";
                break;
            default:
                return; 
        }
        activateButton(key);
    });

    buttons.forEach(button => {
        button.addEventListener('click', function () {
            const key = this.textContent;
            activateButton(key);
        });
    });
});